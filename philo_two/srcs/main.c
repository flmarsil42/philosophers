/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: a42 <a42@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/03 11:10:01 by a42               #+#    #+#             */
/*   Updated: 2020/12/10 11:00:07 by a42              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/philo_two.h"

int			destroy_semaphores(t_data d)
{
	int		ret;

	ret = 0;
	if ((ret = sem_close(d.forks)) && ret < 0)
		return (msg_error("destroy_semaphores(0): error sem_close(forks)\n"));
	if ((ret = sem_close(d.write)) && ret < 0)
		return (msg_error("destroy_semaphores(1): error sem_close(write)\n"));
	if ((ret = sem_close(d.pick)) && ret < 0)
		return (msg_error("destroy_semaphores(2): error sem_close(pick)\n"));
	if ((ret = sem_unlink(S_FORKS)) && ret < 0)
		return (msg_error("destroy_semaphores(3): error sem_unlink(forks)\n"));
	if ((ret = sem_unlink(S_PICK)) && ret < 0)
		return (msg_error("destroy_semaphores(4): error sem_unlink(pick)\n"));
	if ((ret = sem_unlink(S_WRITE)) && ret < 0)
		return (msg_error("destroy_semaphores(5): error sem_unlink(write)\n"));
	return (SUCCESS);
}

int			semaphores_init(t_data *d)
{
	sem_unlink(S_FORKS);
	sem_unlink(S_PICK);
	sem_unlink(S_WRITE);
	if ((d->forks = sem_open(S_FORKS, O_CREAT, 0644, d->n_ph)) == SEM_FAILED)
		return (msg_error("sem_init(0) : erreur sem_open(S_FORKS)\n"));
	if ((d->write = sem_open(S_WRITE, O_CREAT, 0644, 1)) == SEM_FAILED)
		return (msg_error("sem_init(1) : erreur sem_open(S_WRITE)\n"));
	if ((d->pick = sem_open(S_PICK, O_CREAT, 0644, 1)) == SEM_FAILED)
		return (msg_error("sem_init(2) : erreur sem_open(S_PICK)"));
	return (SUCCESS);
}

t_philo		**create_philosophers(t_data *d)
{
	int		i;
	t_philo	**ph;

	if (!(ph = (t_philo **)malloc(sizeof(t_philo *) * d->n_ph)))
		return (NULL);
	i = -1;
	while (++i < d->n_ph)
	{
		ph[i] = (t_philo *)malloc(sizeof(t_philo));
		ph[i]->id = i + 1;
		ph[i]->n_eat = 0;
		ph[i]->t_last_eat = 0;
		ph[i]->data = d;
		ph[i]->sem_id[0] = ph[i]->id + '0';
		ph[i]->sem_id[1] = '\0';
		sem_unlink(ph[i]->sem_id);
		if ((ph[i]->eating = sem_open(ph[i]->sem_id, O_CREAT, 0644, 1))
			== SEM_FAILED)
			return (NULL);
	}
	return (ph);
}

int			main(int ac, char **av)
{
	t_data	d;
	t_philo	**p;

	if (parsing(ac, av, &d))
		return (FAILURE);
	if (semaphores_init(&d))
		return (FAILURE);
	if (!(p = create_philosophers(&d)))
		return (FAILURE);
	if (create_threads(p, &d))
		return (FAILURE);
	if (join_threads(p, &d))
		return (FAILURE);
	if (destroy_semaphores(d))
		return (FAILURE);
	free_all_malloc(&d, p);
	return (SUCCESS);
}
