/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: a42 <a42@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/03 13:42:23 by a42               #+#    #+#             */
/*   Updated: 2020/12/05 15:36:55 by a42              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/philo_two.h"

int		check_error(int i, char *av, t_data *d)
{
	if (only_digit(av))
		return (msg_error("check_error(0) : erreur digit seulement\n"));
	if (ft_strlen(av) > SEC_LEN)
		return (msg_error("check_error(1) : erreur longueur arguments\n"));
	if (i == 1 && ((d->n_ph = ft_atoi(av)) < 2 || d->n_ph > SEC_PH))
		return (msg_error("check_error(2) : erreur argument 1\n"));
	if (i == 2 && ((d->tt_die = ft_atoi(av)) < 1 || d->tt_die > SEC_MS))
		return (msg_error("check_error(3) : erreur argument 2\n"));
	if (i == 3 && ((d->tt_eat = ft_atoi(av)) < 1 || d->tt_eat > SEC_MS))
		return (msg_error("check_error(4) : erreur argument 3\n"));
	if (i == 4 && ((d->tt_sleep = ft_atoi(av)) < 1 || d->tt_sleep > SEC_MS))
		return (msg_error("check_error(5) : erreur argument 4\n"));
	if (i == 5 && ((d->n_max_eat = ft_atoi(av)) < 1 || d->n_max_eat > SEC_MS))
		return (msg_error("check_error(6) : erreur argument 5\n"));
	return (SUCCESS);
}

int		parsing(int ac, char **av, t_data *d)
{
	int i;

	if (!(ac == 5 || ac == 6))
		return (msg_error("parsing(0) : nombre arguments\n"));
	i = 1;
	while (i < ac)
	{
		if (check_error(i, av[i], d))
			return (msg_error("parsing(1) : erreur arguments\n"));
		i++;
	}
	(ac == 5) ? d->n_max_eat = -1 : 0;
	d->dead = 0;
	d->no_hungry = 0;
	d->go = 0;
	return (SUCCESS);
}
