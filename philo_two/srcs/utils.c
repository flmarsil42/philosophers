/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: a42 <a42@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/03 15:10:50 by a42               #+#    #+#             */
/*   Updated: 2020/12/05 15:42:34 by a42              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/philo_two.h"

void			free_all_malloc(t_data *d, t_philo **p)
{
	int			i;

	i = -1;
	while (++i < d->n_ph)
		free(p[i]);
	free(p);
}

void			wait_time_to(u_int64_t tt_x)
{
	u_int64_t	start;

	start = get_time();
	while (get_time() - start < tt_x)
		usleep(100);
}

void			putnbr_buffer(u_int64_t time)
{
	int			i;
	char		buf[100];

	i = 100;
	buf[--i] = ' ';
	if (!time)
	{
		write(1, "0 ", 2);
		return ;
	}
	while (time)
	{
		buf[--i] = (time % 10) + '0';
		time /= 10;
	}
	write(1, &buf[i], 100 - i);
}

void			screen_msg(t_philo *phil, char *msg)
{
	sem_wait(phil->data->write);
	if (!phil->data->dead)
	{
		putnbr_buffer(get_time());
		putnbr_buffer(phil->id);
		write(1, msg, ft_strlen(msg));
	}
	sem_post(phil->data->write);
}

u_int64_t		get_time(void)
{
	static struct timeval	start = {0, 0};
	struct timeval			tv;

	if (!start.tv_sec)
		gettimeofday(&start, NULL);
	gettimeofday(&tv, NULL);
	return (((tv.tv_sec - start.tv_sec) * (u_int64_t)1000) +
			((tv.tv_usec - start.tv_usec) / 1000));
}
