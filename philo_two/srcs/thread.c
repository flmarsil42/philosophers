/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   thread.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: a42 <a42@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/03 14:20:25 by a42               #+#    #+#             */
/*   Updated: 2020/12/05 19:44:14 by a42              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/philo_two.h"

void			*eat(t_philo *p)
{
	sem_wait(p->data->pick);
	sem_wait(p->data->forks);
	screen_msg(p, FORK);
	sem_wait(p->data->forks);
	sem_post(p->data->pick);
	if (!p->data->dead)
	{
		sem_wait(p->eating);
		screen_msg(p, FORK);
		screen_msg(p, EAT);
		p->t_last_eat = get_time();
		wait_time_to(p->data->tt_eat);
		p->n_eat++;
	}
	sem_post(p->eating);
	sem_post(p->data->forks);
	sem_post(p->data->forks);
	return (NULL);
}

void			*phil_life_controller(void *p)
{
	t_philo *phil;

	phil = (t_philo *)p;
	while (!phil->data->dead && phil->n_eat != phil->data->n_max_eat)
	{
		sem_wait(phil->eating);
		if (!phil->data->dead && phil->n_eat != phil->data->n_max_eat &&
			get_time() - phil->t_last_eat > (u_int64_t)phil->data->tt_die)
		{
			screen_msg(phil, DEAD);
			phil->data->dead = 1;
			sem_post(phil->eating);
			break ;
		}
		sem_post(phil->eating);
		usleep(100);
	}
	return (NULL);
}

void			*phil_start(void *p)
{
	t_philo		*phil;
	pthread_t	control;

	phil = (t_philo *)p;
	pthread_create(&control, NULL, phil_life_controller, phil);
	while (!phil->data->go)
		;
	while (!phil->data->dead)
	{
		eat(phil);
		if (!phil->data->dead && phil->n_eat == phil->data->n_max_eat)
		{
			sem_wait(phil->data->write);
			phil->data->no_hungry++;
			sem_post(phil->data->write);
			break ;
		}
		screen_msg(p, SLEEP);
		wait_time_to(phil->data->tt_sleep);
		screen_msg(p, THINK);
	}
	pthread_join(control, NULL);
	sem_close(phil->eating);
	sem_unlink(phil->sem_id);
	return (NULL);
}

int				create_threads(t_philo **p, t_data *d)
{
	int			i;

	i = -1;
	while (++i < d->n_ph)
		if (pthread_create(&(p[i]->thread), NULL, &phil_start, (void *)p[i]))
			return (msg_error("create_thread(0) : erreur création thread\n"));
	d->go = 1;
	return (SUCCESS);
}

int				join_threads(t_philo **p, t_data *d)
{
	int			i;
	int			ret;

	while (!d->dead && d->no_hungry < d->n_ph)
		usleep(100);
	i = -1;
	ret = 0;
	while (++i < d->n_ph)
	{
		if ((ret = sem_post(d->forks)) && ret < 0)
			return (msg_error("join_threads(0) : erreur sem_post\n"));
		if (pthread_join(p[i]->thread, NULL))
			return (msg_error("join_threads(1) : erreur join thread\n"));
	}
	return (SUCCESS);
}
