/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo_two.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: a42 <a42@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/03 11:10:05 by a42               #+#    #+#             */
/*   Updated: 2020/12/12 01:37:39 by a42              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_TWO_H
# define PHILO_TWO_H

# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <pthread.h>
# include <sys/time.h>
# include <semaphore.h>
# include <fcntl.h>
# include <sys/types.h>

# define FORK			" has taken a fork\n"
# define EAT			" is eating\n"
# define SLEEP			" is sleeping\n"
# define THINK			" is thinking\n"
# define DEAD			" died\n"

# define S_FORKS		"sem_forks"
# define S_PICK			"sem_pick_forks"
# define S_WRITE		"sem_write"

# define FAILURE 		1
# define SUCCESS 		0
# define SEC_LEN 		7
# define SEC_PH 		200
# define SEC_MS 		100000

typedef struct			s_data
{
	int					n_ph;
	int					tt_eat;
	int					tt_die;
	int					tt_sleep;
	int					n_max_eat;
	int					no_hungry;
	int					dead;
	int					go;
	sem_t				*pick;
	sem_t				*forks;
	sem_t				*write;
}						t_data;

typedef struct			s_philo
{
	int					id;
	int					n_eat;
	unsigned long long	t_last_eat;
	t_data				*data;
	char				sem_id[2];
	sem_t				*eating;
	pthread_t			thread;
}						t_philo;

void					free_all_malloc(t_data *d, t_philo **p);
int						parsing(int ac, char **av, t_data *d);
t_philo					**create_philosophers(t_data *d);

int						create_threads(t_philo **p, t_data *d);
int						join_threads(t_philo **p, t_data *d);

void					wait_time_to(u_int64_t tt_x);
u_int64_t				get_time(void);
int						msg_error(char *s);
int						only_digit(char *av);

void					screen_msg(t_philo *phil, char *msg);

int						ft_atoi(const char *str);
int						ft_strlen(char *s);

#endif
