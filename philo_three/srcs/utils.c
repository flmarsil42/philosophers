/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: a42 <a42@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/03 15:10:50 by a42               #+#    #+#             */
/*   Updated: 2020/12/10 09:59:26 by a42              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/philo_three.h"

void			wait_time_to(u_int64_t tt_x)
{
	u_int64_t	start;

	start = get_time();
	while (get_time() - start < tt_x)
		usleep(100);
}

void			putnbr_buffer(u_int64_t time)
{
	int			i;
	char		buf[100];

	i = 100;
	buf[--i] = ' ';
	if (!time)
	{
		write(1, "0 ", 2);
		return ;
	}
	while (time)
	{
		buf[--i] = (time % 10) + '0';
		time /= 10;
	}
	write(1, &buf[i], 100 - i);
}

void			screen_msg(int id, char *msg)
{
	sem_wait(g_write);
	sem_wait(g_dead);
	sem_post(g_dead);
	putnbr_buffer(get_time());
	putnbr_buffer(id);
	write(1, msg, ft_strlen(msg));
	sem_post(g_write);
}

u_int64_t		get_time(void)
{
	static struct timeval	start = {0, 0};
	struct timeval			tv;

	if (!start.tv_sec)
		gettimeofday(&start, NULL);
	gettimeofday(&tv, NULL);
	return (((tv.tv_sec - start.tv_sec) * (u_int64_t)1000) +
			((tv.tv_usec - start.tv_usec) / 1000));
}
