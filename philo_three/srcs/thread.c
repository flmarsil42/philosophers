/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   thread.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: a42 <a42@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/03 14:20:25 by a42               #+#    #+#             */
/*   Updated: 2020/12/10 09:58:59 by a42              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/philo_three.h"

void			*eat(t_philo *p)
{
	sem_wait(g_pick);
	sem_wait(g_forks);
	screen_msg(p->id, FORK);
	sem_wait(g_forks);
	sem_post(g_pick);
	sem_wait(p->eating);
	screen_msg(p->id, FORK);
	screen_msg(p->id, EAT);
	p->t_last_eat = get_time();
	wait_time_to(p->data.tt_eat);
	sem_post(g_forks);
	sem_post(g_forks);
	p->n_eat++;
	sem_post(p->eating);
	return (NULL);
}

void			*phil_life_controller(void *p)
{
	t_philo		*phil;

	phil = (t_philo *)p;
	while (1)
	{
		sem_wait(phil->eating);
		if (get_time() - phil->t_last_eat > (u_int64_t)phil->data.tt_die)
		{
			if (phil->n_eat != phil->data.n_max_eat)
			{
				screen_msg(phil->id, DEAD);
				sem_wait(g_dead);
				sem_post(phil->eating);
				exit(1);
			}
		}
		sem_post(phil->eating);
		usleep(100);
	}
	return (NULL);
}

void			phil_start(t_philo p, pid_t *ph_pid)
{
	pthread_t	control;

	free(ph_pid);
	p.t_last_eat = get_time();
	pthread_create(&control, NULL, phil_life_controller, (void *)&p);
	while (1)
	{
		eat(&p);
		if (p.n_eat == p.data.n_max_eat)
			exit(0);
		screen_msg(p.id, SLEEP);
		wait_time_to(p.data.tt_sleep);
		screen_msg(p.id, THINK);
	}
}
